#filesystems.py
#Gibson Mulonga
#November 9, 2015
import HTML
import os
import collections


#Module for extracting data from a file (chesssamba.conf) and converting it to a table in html
def createTable():
    #extracts the data and converts it to table in html
    
    #Production infiles
    chessInfile = os.path.dirname(os.path.abspath(__file__))+"/chesssamba.conf"
    smbInfile = os.path.dirname(os.path.abspath(__file__))+"/samba.conf"
    cesronlInfile = os.path.dirname(os.path.abspath(__file__))+"/cesronl.conf"
    chessdaqInfile = os.path.dirname(os.path.abspath(__file__))+"/chessdaq.conf"
    erp101Infile = os.path.dirname(os.path.abspath(__file__))+"/erp101.conf"
    nfsInfile = os.path.dirname(os.path.abspath(__file__))+"/auto.nfs"
    cdatInfile = os.path.dirname(os.path.abspath(__file__))+"/auto.cdat"

    
    #Development outfile
    outfile = os.path.dirname(__file__)+"systems.html"
    
    #Production outfile
    #outfile = "/nfs/classe/www/html/private/computing/filesystems.html"
    
    
    #Table names
    sambaTbl = 'General Samba and NFS'
    chessTbl = 'chesssamba'
    cesrTbl = 'cesronl'
    chessdaqTbl = 'chessdaq'
    erpTbl = 'erp101'
   
    #dictionaries to hold the table data
    chessDict = {}
    cesronlDict = {}
    chessdaqDict = {}
    erp101Dict = {}
    smbDict = {}
    smbCommentDict = {}
    errors = []
    share = ''
    sortDict = {}
    
    #Open infiles
    chesstext = open(chessInfile)
    cesronlText = open(cesronlInfile)
    chessdaqText = open(chessdaqInfile)
    erp101Text = open(erp101Infile)
    smbtext = open(smbInfile)
    nfsText = open(nfsInfile)
    cdatText = open(cdatInfile)
    
    
    linuxPath = ''
    tableData = []     #list to hold table data for the html file

    
    
    #cdatInfile = "/home/glm83/Downloads/auto.cdat"
    #nfsInfile = "/home/glm83/Downloads/auto.nfs"
    #smbconfInfile = "/etc/cluster/samba/samba:samba/smb.conf"
    #outfile = "/nfs/classe/www/html/private/computing/central.html"
    
    
    
    
    
    
    
    
    
    sortDict.update({'/home':('/home' + '!!' + '\\\samba.classe.cornell.edu\\home' + '!!' + 'cifs://samba.classe.cornell.edu/home'
                              + '!!' + '/home' + '!!' +'homes:/mnt/homes/classe' + '!!' + '-'+ '!!' + '-'+ '!!' + '-'+ '!!' + '-')})
    
    
    
    #infile = "/etc/cluster/samba/samba:samba/chesssamba.conf"
    #outfile = "/nfs/classe/www/html/private/computing/chesssamba.html"
   
   
   
    
    
    
    

    #code to parse data from smb.conf
    for line in smbtext:   #loop through the entire file line by line 
        shareIndex = line.find("[")  #find first brace
        commentIndex = line.find("comment = ")   #index of comment
        pathIndex = line.find("path = ") #index of path
        if(shareIndex!=-1):
            share = line[shareIndex+1:-2].strip() #share
        if(commentIndex!=-1):
            comment = line[commentIndex+10:].strip()  #comment
        if(pathIndex!=-1):
            path = line[pathIndex+7:].strip()  #linux path
            smbDict.update({path:share})
            smbCommentDict.update({path:comment})
            
    
    
    
    #loop to read auto.cdat
    for line in cdatText:   #loop through the entire file line by line 
        space = line.find(" ")  #find space
        forwardSlash = line.find("/")
        if(space!=-1):
            linuxPath =  line[0:space]  #linux path
            linuxPath = "/cdat/" +linuxPath
            linuxPath = linuxPath.strip()
            fileSystem = line[space+1:]
            try:
                comment = smbCommentDict[linuxPath]
                windowsPath = "\\\samba.classe.cornell.edu\\" + smbDict[linuxPath] 
                osxPath = "cifs://samba.classe.cornell.edu/" + smbDict[linuxPath]
            except:
                comment =linuxPath
                windowsPath = "-" 
                osxPath = "-" 
            
            
            row = comment + '!!' + windowsPath + '!!' + osxPath + '!!' + linuxPath 
            try:
                s = makeString(runCommand(linuxPath))   #get the fileSystem, size, amount used....
                if(s.find("%")==-1):
                    s = makeString(runCommand(linuxPath))
                if (len(s.strip())<10):
                    e = pathUsed+ '!!' + '-'
                    errors.append(e.split('!!'))
                else:
                    sortDict.update({linuxPath:(row+'!!'+s)})
                #tableData.append(row.split("!!") + s.split("!!"))   #take the row and filesystem data and append to a row in the table array
            except:
                e = linuxPath + '!!' + fileSystem 
                errors.append(e.split('!!'))
                
                
                
    #loop to read auto.nfs
    for line in nfsText:   #loop through the entire file line by line 
        space = line.find(" ")  #find space
        forwardSlash = line.find("/")
    
        if(space!=-1):
            linuxPath =  line[0:space]  #linux path
            linuxPath = "/nfs/" +linuxPath
            linuxPath = linuxPath.strip()
            fileSystem = line[space:]
            #print "Here: %sTT" %linuxPath
            try:
                comment = smbCommentDict[linuxPath]
                #print "Comment: %s" %comment
                windowsPath = "\\\samba.classe.cornell.edu\\" + smbDict[linuxPath]
                osxPath = "cifs://samba.classe.cornell.edu/" + smbDict[linuxPath]
            except:
                comment =linuxPath
                windowsPath = "-" 
                osxPath = "-" 
            row = comment + '!!' + windowsPath + '!!' + osxPath + '!!' + linuxPath 
            try:
                s = makeString(runCommand(linuxPath))   #get the fileSystem, size, amount used....
                #print "Made string: %s" %s
                if(s.find("%")==-1):
                    s = makeString(runCommand(linuxPath))
                if (len(s.strip())<10):
                    e = pathUsed+ '!!'+ '-'
                    errors.append(e.split('!!'))
                else:
                    sortDict.update({linuxPath:(row+'!!'+s)})
                #tableData.append(row.split("!!") + s.split("!!"))   #take the row and filesystem data and append to a row in the table array
            except:
                e = linuxPath + '!!' + fileSystem
                str(e)
                errors.append(e.split('!!'))
            #s = makeString(runCommand(line[path+7:]))   #get the fileSystem, size, amount used....
            #tableData.append(row.split("!!") + s.split("!!"))   #take the row and filesystem data and append to a row in the table array   
    
    
    for line in chesstext:   #loop through the entire file line by line 
        first = line.find("[")  #find first brace
        last = line.find("]")   #find last brace
        comment = line.find("comment = ")   #index of comment
        path = line.find("path = ") #index of path
        if(first!=-1):
            row = "\\\chesssamba.classe.cornell.edu\\" + line[first+1:last]  #windows path
            mac = "!!"+"smb://chesssamba.classe.cornell.edu/" + line[first+1:last]  #mac os x path
            row = row+mac #windows path concatenated with mac os x path
        if(comment!=-1):
            row = line[comment+10:]  + "!!" +row  #comment
        if(path!=-1):
            pathUsed = line[path+7:]
            chessDict.update({pathUsed:(row)}) #dict to sort the html generated based on linuxPath
            
                    
    
    for line in cesronlText:   #loop through the entire file line by line 
        first = line.find("[")  #find first brace
        last = line.find("]")   #find last brace
        comment = line.find("comment = ")   #index of comment
        path = line.find("path = ") #index of path
        if(first!=-1):
            row = "\\\cesronl.classe.cornell.edu\\" + line[first+1:last]  #windows path
            mac = "!!"+"smb://cesronl.classe.cornell.edu/" + line[first+1:last]  #mac os x path
            row = row+mac #windows path concatenated with mac os x path
        if(comment!=-1):
            row = line[comment+10:]  + "!!" +row  #comment
        if(path!=-1):
            pathUsed = line[path+7:]
            cesronlDict.update({pathUsed:(row)}) #dict to sort the html generated based on linuxPath
            
    
    for line in chessdaqText:   #loop through the entire file line by line 
        first = line.find("[")  #find first brace
        last = line.find("]")   #find last brace
        comment = line.find("comment = ")   #index of comment
        path = line.find("path = ") #index of path
        if(first!=-1):
            row = "\\\chessdaq.classe.cornell.edu\\" + line[first+1:last]  #windows path
            mac = "!!"+"smb://chessdaq.classe.cornell.edu/" + line[first+1:last]  #mac os x path
            row = row+mac #windows path concatenated with mac os x path
        if(comment!=-1):
            row = line[comment+10:]  + "!!" +row  #comment
        if(path!=-1):
            pathUsed = line[path+7:]
            chessdaqDict.update({line[path+7:]:(row)}) #dict to sort the html generated based on linuxPath
            
    
    for line in erp101Text:   #loop through the entire file line by line 
        first = line.find("[")  #find first brace
        last = line.find("]")   #find last brace
        comment = line.find("comment = ")   #index of comment
        path = line.find("path = ") #index of path
        if(first!=-1):
            row = "\\\erp101.classe.cornell.edu\\" + line[first+1:last]  #windows path
            mac = "!!"+"smb://erp101.classe.cornell.edu/" + line[first+1:last]  #mac os x path
            row = row+mac #windows path concatenated with mac os x path
        if(comment!=-1):
            row = line[comment+10:]  + "!!" +row  #comment
        if(path!=-1):
            pathUsed = line[path+7:]           
            erp101Dict.update({line[path+7:]:(row)}) #dict to sort the html generated based on linuxPath
            
    
    
    
       
    
    notFound = HTML.table(errors,header_row=['LINUX PATH', 'FILE SYSTEM'])
    
    for i in range(len(notFound)):
        notFound = notFound.replace('<TD>&nbsp;</TD>','')
    
    
    
    backToTop = '  <div class="back_top" ><a href="#top">Back To Top</a></div>'

    
    
    htmlcode = head + tableOfContents +formatTable(sambaTbl, '', sortDict,"samba_and_nfs") +  formatSmallTable(chessTbl, backToTop, chessDict, '"chesssamba"') + formatSmallTable(cesrTbl, backToTop, cesronlDict, '"cesronl"') + formatSmallTable(chessdaqTbl,backToTop, chessdaqDict, '"chessdaq"') + formatSmallTable(erpTbl, backToTop, erp101Dict, '"erp"') + end + notFoundHeader+ notFound + end2
    
    
    with open(outfile, 'w') as myFile:     #create a html file called samba and save it
        myFile.write(htmlcode)

def formatTable(tableName, backToTop, dictTosort, link):
    #create html table and add formatting. Also remove extra column at the end
    tableData = []
    sortedDict = collections.OrderedDict(sorted(dictTosort.items()))
    for i in range(len(sortedDict)):
        tableData.append(sortedDict.values()[i].split('!!'))
    
    t = HTML.table(tableData,header_row=['COMMENT','WINDOWS PATH', 'MAC OS X PATH','LINUX PATH',
                                                'FILESYSTEM','TOTAL SIZE', 'AMOUNT USED', 'AMOUNT FREE',
                                                'PERCENT USED', 'BACKED UP'],)  # a header row for the table, in bold
    ht = str(t)
    
    column1 = ht.find("COMMENT<")
    ht = ht[0:column1-1]+' class="sorttable_alpha"'+ht[(column1-1):]
    for i in range(len(ht)):
        stripes = ht.find('<TR>')
        if(stripes!=-1): 
            ht = ht[0:stripes+3] + ' class="alt"'+ht[(stripes+3):]
     #remove spare column at the end            
    for i in range(len(ht)):
        ht = ht.replace('<TD>&nbsp;</TD>','')
        
    ins = ht.find("<TABLE")
    data = '<h1' + ' id= '+ link + '>' +tableName+'</h1>' + ht[0:ins+6]+ sort + ht[(ins+6):] + '<br><br>'
    return data


def formatSmallTable(tableName, backToTop, dictTosort, link):
    #create html table and add formatting. Also remove extra column at the end
    tableData = []
    sortedDict = collections.OrderedDict(sorted(dictTosort.items()))
    for i in range(len(sortedDict)):
        tableData.append(sortedDict.values()[i].split('!!'))
    
    t = HTML.table(tableData,header_row=['COMMENT','WINDOWS PATH', 'MAC OS X PATH',],)  # a header row for the table, in bold
    ht = str(t)
    
    column1 = ht.find("COMMENT<")
    ht = ht[0:column1-1]+' class="sorttable_alpha"'+ht[(column1-1):]
    for i in range(len(ht)):
        stripes = ht.find('<TR>')
        if(stripes!=-1): 
            ht = ht[0:stripes+3] + ' class="alt"'+ht[(stripes+3):]
    #remove spare column at the end            
    for i in range(len(ht)):
        ht = ht.replace('<TD>&nbsp;</TD>','')
        
    ins = ht.find("<TABLE")
    data = '<h1' + ' id= '+ link + '>' +tableName+'</h1>' + ht[0:ins+6]+ sort + ht[(ins+6):] + '<br><br>'
    return data


def runCommand(path):
    #Takes the path and runs the 'df -hP' command on it and returns the second line of the output from cmd
    path = path.strip()
    p = os.popen('df -hP '+ path)    #run the command
    #print path
    s = p.readlines()   #take the output from cmd and convert it to a string array line by line
    #print s[1]
    return s[1]     #return the second line


def makeString(s):
    #takes input from runCommand() and splits it into a string separating each value by '!!' for easier splitting
    table2 = ""     #string to hold the values as they are being sliced from s
    tmp = s     #temporary variable
    index = s.find(" ")     #find first space
    while (index!=-1):  #so long as the string has a space in it
        data = tmp[0:index]
        tmp = tmp[index:].strip()
        table2 = table2+data + "!!"
        index = tmp.find(" ")
    return table2


def nfsCommand():
    #runs the "ypcat -k amd.nfs |awk '{print $1}'" command and returns the values
    p = os.popen("ypcat -k amd.nfs |awk '{print $1}'")    #run the command
    s = p.readlines()   #take the output from cmd and convert it to a string array line by line
    return s    #return the cmd output


def cdatCommand():
    #runs the "ypcat -k amd.cdat |awk '{print $1}'" command and returns the values
    p = os.popen("ypcat -k amd.cdat |awk '{print $1}'")    #run the command
    s = p.readlines()   #take the output from cmd and convert it to a string array line by line
    return s    #return the cmd output
    
 
style = ' <style> table.sortable th:not(.sorttable_sorted):not(.sorttable_sorted_reverse):not(.sorttable_nosort):after{content: " \\25B4\\25BE" }tr:hover td{background-color:#A7C942;background:-moz-linear-gradient( center top, #A7C942 10%, #A7C942 100% );}p{ font-family:Verdana, Geneva, sans-serif; font-size:.9em;text-align:justify;}th:not(.sorttable_nosort){cursor:pointer; text-decoration: underline;}h1{ font-family:"Trebuchet MS", Arial, Helvetica, sans-serif; font-size:1.4em;text-align:left;}td,th {border:1px solid #98bf21;padding:3px 7px 2px 7px;}th, head{font-family:"Trebuchet MS", Arial, Helvetica, sans-serif;font-size:1.4em; text-align:center; padding-top:5px; padding-bottom:4px; background-color:#A7C942; color:#fff;}tr.alt td {color:#000;background-color:#EAF2D3;} .back_top {float: right;} #back_to_top {right: 0px;top: 51.6%;position: fixed;font-family: Arial,Verdana,sans-serif;z-index: 1000;top: 25%;}</style>'
title = '<title>CLASSE Central File Systems</title><h1>CLASSE Central File Systems</h1>'
heading = '<p>Please see below for a listing of CLASSE file systems accessible over Samba and NFS.<br>If you would like assistance determining where to store your data or would like a new file system created, please email <a href="mailto:service-classe@cornell.edu">service-classe@cornell.edu</a><br>For more on data stewardship at CLASSE, please see <a href="https://wiki.classe.cornell.edu/Computing/DataStewardship">https://wiki.classe.cornell.edu/Computing/DataStewardship</a></p>'
head = '<!DOCTYPE html><html><head><script src="sortTable.js"></script>' +title+ heading+style + '</head><body>'
tableOfContents = '<h1>Table of Contents</h1><ul><li><a href="#samba_and_nfs">General Samba and NFS</a></li><li><a href="#chesssamba">ChessSamba</a></li><li><a href="#cesronl">Cesronl</a></li><li><a href="#chessdaq">Chessdaq</a></li><li><a href="#erp">Erp101</a></li><li><a href="#not_found">Not Found</a></li><br><br>'
notFoundHeader = '<br><br><h1 id="not_found">Not Found</h1><p>The following filesystems are unavailable for any number of reasons.<br> For example, some critical control system filesystems are not accessible to the process that generated this page. Please email <a href="mailto:service-classe@cornell.edu">service-classe@cornell.edu</a> with any questions or concerns.</p>'
sort = ' style="background-color:white;" class="sortable" '
end = '</TABLE>'
toTop = '<aside id="back_to_top"><ul><li><a href="#top">Back Top</a></li></ul></aside>'
nextTable = '<br><br>'
end2 = '</TABLE></body></html>'






   

if __name__ == "__main__":
    createTable()
    
    